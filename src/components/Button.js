function Button(props) {
  return (
    <button
      className={
        "py-4 block self-stretch text-center px-6 rounded-full text-white transition bg-primary-500 hover:bg-primary-600 hover:font-bold " +
        props.className
      }
    >
      {props.children}
    </button>
  );
}

export { Button };
