import { SidebarLink } from "./SidebarLink";
import { Button } from "./Button";
import { HomeIcon } from "./icons/Home";
import { UserSummaryButton } from "./UserSummaryButton";
function Sidebar(props) {
  return (
    <div
      className={
        `h-screen md:fixed w-full flex flex-col gap-6 p-4 justify-between ${props.className}`
      }
    >
      <div className="flex flex-col gap-4">
        <SidebarLink>
          <HomeIcon className="fill-black dark:fill-white" />
          <span className="text-3xl font-bold">Home</span>
        </SidebarLink>
        <SidebarLink>
          <HomeIcon className="fill-black dark:fill-white" />
          <span className="text-3xl font-bold">Profile</span>
        </SidebarLink>

        <Button>Twittami</Button>
      </div>
      <UserSummaryButton />
    </div>
  );
}

export { Sidebar };
