import { TweetCardFooterIcon } from "./TweetCardFooterIcon";
function TweetCard(props) {
  return (
    <div className="flex gap-3 transition hover:bg-black/5 dark:hover:bg-white/5 ">
      <div className="h-[50px] w-[50px] rounded-full bg-cyan-400 flex-none"></div>
      <div className="">
        <div className="flex gap-2 items-end">
          <h1 className="font-bold">Pippo Pluto</h1>
          <span className="text-sm text-black/40 dark:text-white/40">@pippo_pluto</span>
          <i className="h-[2px] w-[2px] bg-black dark:bg-white rounded-full self-center"></i>
          <span className="text-black/60 dark:text-white/60">2h</span>
        </div>
        <div>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Explicabo
          minima, error animi ullam laudantium nostrum nam itaque distinctio aut
          ipsum voluptatem fuga, consequatur iure incidunt omnis placeat
          molestiae sed enim?
        </div>
        <div className="flex justify-between items-center">
          <TweetCardFooterIcon color="red">34</TweetCardFooterIcon>
          <TweetCardFooterIcon color="green"/>
          <TweetCardFooterIcon color="purple" />
          <TweetCardFooterIcon color="yellow" />
        </div>
      </div>
    </div>
  );
}

export { TweetCard };
