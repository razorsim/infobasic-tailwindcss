function SidebarLink(props) {
  return (
    <a
      href="/#"
      className="inline-flex rounded-full hover:bg-white/10 transition py-4 pl-4 pr-8 items-center gap-3 leading-none"
    >
      {props.children}
    </a>
  );
}

export { SidebarLink };
