import { Button } from "./Button";
import { TweetCardFooterIcon } from "./TweetCardFooterIcon";

export const TweetSendCard = (props) => {
  return (
    <div className="flex gap-2">
      <div className="flex-none w-[60px] h-[60px] rounded-full bg-black/10 dark:bg-white/10 hover:brightness-75 transition">
        <img
          className="w-full h-full rounded-full object-cover"
          src="https://media.wired.co.uk/photos/60c8730fa81eb7f50b44037e/3:2/w_3329,h_2219,c_limit/1521-WIRED-Cat.jpeg"
          alt="avatar"
        />
      </div>
      <div className="w-full">
        <textarea className="w-full  py-2 outline-none focus:outline-none bg-transparent placeholder:text-black/30 dark:placeholder:text-white/30 text-xl placeholder:font-semibold"></textarea>
        <div className="flex justify-between items-center">
          <div className="flex gap-3">
            <TweetCardFooterIcon color="purple" />
            <TweetCardFooterIcon color="purple" />
            <TweetCardFooterIcon color="purple" />
            <TweetCardFooterIcon color="purple" />
          </div>
          <Button>Tweet</Button>
        </div>
      </div>
    </div>
  );
};
