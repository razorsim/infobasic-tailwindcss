import { HomeIcon } from "./icons/Home";

const colorToClasses = new Map([
  ["blue-text", "group-hover:text-blue-500"],
  ["blue-bg", "group-hover:bg-blue-500/20"],
  ["blue-fill", "group-hover:fill-blue-500"],
  ["red-text", "group-hover:text-red-500"],
  ["red-bg", "group-hover:bg-red-500/20"],
  ["red-fill", "group-hover:fill-red-500"],
  ["green-text", "group-hover:text-green-500"],
  ["green-bg", "group-hover:bg-green-500/20"],
  ["green-fill", "group-hover:fill-green-500"],
  ["yellow-text", "group-hover:text-yellow-500"],
  ["yellow-bg", "group-hover:bg-yellow-500/20"],
  ["yellow-fill", "group-hover:fill-yellow-500"],
  ["purple-text", "group-hover:text-purple-500"],
  ["purple-bg", "group-hover:bg-purple-500/20"],
  ["purple-fill", "group-hover:fill-purple-500"],
]);

export const TweetCardFooterIcon = (props) => {
  return (
    <button className="flex items-center justify-center gap-1 group transition">
      <div
        className={`rounded-full flex h-8 w-8  items-center justify-center transition ${colorToClasses.get(
          props.color + "-bg"
        )}`}
      >
        <HomeIcon
          className={`fill-black/40 dark:fill-white/40 transition  h-5 w-5 ${colorToClasses.get(
            props.color + "-fill"
          )}`}
        />
      </div>
      <span
        className={`text-xs text-black/40 dark:text-white/40 transition ${colorToClasses.get(
          props.color + "-text"
        )}`}
      >
        {props.children}
      </span>
    </button>
  );
};
