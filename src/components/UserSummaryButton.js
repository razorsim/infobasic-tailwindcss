function UserSummaryButton(props) {
  return (
    <div className="flex justify-center gap-3 items-center">
      <div className="min-h-[40px] min-w-[40px] rounded-full bg-gray-200"></div>
      <div className="min-w-[120px]">
        <p className="font-bold text-ellipsis overflow-hidden ...">
          Raz | 0xRaz.eth
        </p>
        <p className="text-sm font-light text-black/40 text-ellipsis overflow-hidden ...">
          @Razor_SiM
        </p>
      </div>
      <button className="flex items-center justify-center gap-0.5 h-8 w-8 rounded-full hover:bg-black/10 transition">
        <span className="bg-black block h-1 w-1 rounded-full"></span>
        <span className="bg-black block h-1 w-1 rounded-full"></span>
        <span className="bg-black block h-1 w-1 rounded-full"></span>
      </button>
    </div>
  );
}

export { UserSummaryButton };
