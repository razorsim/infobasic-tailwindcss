module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  darkMode: "class",
  theme: {
    extend: {
      colors: {
        primary: {
          500:"rgb(120, 86, 255)",
          600:"rgb(108, 77, 230)",
        },
      }
    },
  },
  plugins: [],
};
